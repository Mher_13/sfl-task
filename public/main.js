const mainEvent = componentEvent();
body = document.querySelector('body');

//render
const render = (e) => {
  mainEvent.unmount();

  const path = getPathname();
  
  switch (path) {
    case 'users':
      renderUsersPage(path);
      break;
    case 'user':
      renderUserPage(path);
      break;
    case 'repos':
      renderReposPage(path);
      break;
    default:
      return pushUrl('users');
  }

  mainEvent.mount(path);
}

render();