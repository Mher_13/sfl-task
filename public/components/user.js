const repoEvent = componentEvent();

const getUserRepos = () => {

  let page = 1;
  let per_page = 2;

  const get = async () => {
    const login = getPathname(2);
    const repos = await makeAPIGetRequest(
      [AppConstants.network.network_request_url.USERS, login, AppConstants.network.network_request_url.REPOS], {
        query: {page, per_page}
      }
    );

    repos && renderRepos(repos);
  }

  return {
    getCurrentPage: () => page,
    setQuery: (props={}) => {
      page = props.page || page;
      return get();
    }
  }
}

const initUserRepos = getUserRepos();

const getUser = async (id) => {
  const login = getPathname(2);
  const user = await makeAPIGetRequest(
    [AppConstants.network.network_request_url.USERS, login],
  );
    
  initUserRepos.setQuery();
  user && renderUser(user, id);
}

const createReposNextPrevButton = () => {
  const div = createElement({
    type: 'div',
    className: 'prev-next-buttons'
  });
  const prevButton = createElement({
    type: 'button',
    className: 'prev',
    innerHTML: 'prev'
  });
  const nextButton = createElement({
    type: 'button',
    className: 'next',
    innerHTML: 'next'
  });

  const pageIndex = initUserRepos.getCurrentPage();

  prevButton.onclick = () => pageIndex>0 && initUserRepos.setQuery({q: this.value, page: pageIndex - 1});
  nextButton.onclick = () => pageIndex>0 && initUserRepos.setQuery({q: this.value, page: pageIndex + 1});

  div.appendChild(prevButton);
  div.appendChild(nextButton);

  return div;
}

const renderRepos = (repos) => {
  repoEvent.unmount();

  const userPage = document.querySelector('.user-page');
  const reposListDiv = createElement({
    type: 'div',
    className: 'repos-list-content'
  });
  const reposListId = 'repos-list-element';

  const reposListElement = createElement({
    type: 'div',
    className: 'repos-list-element',
    id: reposListId
  });

  repos.forEach(repo => {
    const div = createElement({
      type: 'div'
    });
    const p = createElement({
      type: 'p',
      innerHTML: repo.name
    });

    div.appendChild(p);
    reposListDiv.appendChild(div);
    
  });

  repoEvent.mount(reposListId);
  
  reposListElement.appendChild(createReposNextPrevButton());
  reposListElement.appendChild(reposListDiv);
  userPage.appendChild(reposListElement);
}

const renderUser = (user, id) => {
  const userElement = createElement({type: 'section', className: 'user-page', id});
  const userContent = createElement({type: 'div', className: 'user-page-content'});

  const goBackDiv = createElement({type: 'p', className: 'go-back', innerHTML: 'Back to Users'});
  goBackDiv.onclick = goBack;
  
  const userMedia = createElement({type: 'div'});
  const userMediaChild = [
    createElement({type: 'img', atr: {name: 'src', value: user.avatar_url}}),
    createElement({type: 'p', innerHTML: user.name})
  ]
  
  appendChildCustom(userMedia, userMediaChild);

  const userInfo = createElement({type: 'div'});
  const userInfoChild = [
    createElement({type: 'p', innerHTML: `blog ${user.blog}`}),
    createElement({type: 'p', innerHTML: `company ${user.company}`}),
    createElement({type: 'p', innerHTML: `followers ${user.followers}`}),
    createElement({type: 'p', innerHTML: `following ${user.following}`}),
    createElement({type: 'p', innerHTML: `location ${user.location}`})
  ]

  appendChildCustom(userInfo, userInfoChild);
  
  userContent.appendChild(userMedia);
  userContent.appendChild(userInfo);
  userElement.appendChild(goBackDiv);
  userElement.appendChild(userContent);
  body.appendChild(userElement);
}

renderUserPage = (id) => {
  getUser(id)
}