const userListEvent = componentEvent();

const getUsers = () => {
  let q,page;
  let per_page = '20';
  
  const get = async () => {
    const users = await makeAPIGetRequest(
      [AppConstants.network.network_request_url.SEARCH, AppConstants.network.network_request_url.USERS], {
        query: {q, page, per_page}
      }
    );

    renderUsers(users.items);
  }

  return {
    getCurrentPage: () => page,
    setQuery: (props) => {
      q = props.q || q;
      page = props.page || page;
      return get();
    }
  }
}

let initUsers = getUsers();

renderUsersPage = (id) => {
  const usersPage = createElement({
    type: 'section',
    className: 'users-page',
    id
  });
  const usersPageContent = createElement({
    type: 'div',
    className: 'users-page-content'
  });
  const input = createElement({
    type: 'input',
    className: 'search-input'
  });

  usersPageContent.appendChild(input);

  input.addEventListener("keyup", function (e) {
    if (e.keyCode === 13) {
      e.preventDefault();

      initUsers.setQuery({q: this.value, page: 1});
    }
  })

  usersPage.appendChild(usersPageContent);
  body.appendChild(usersPage);
}

const createNextPrevButton = () => {
  const div = createElement({
    type: 'div',
    className: 'prev-next-buttons'
  });
  const prevButton = createElement({
    type: 'button',
    className: 'prev',
    innerHTML: 'prev'
  });
  const nextButton = createElement({
    type: 'button',
    className: 'next',
    innerHTML: 'next'
  });

  const pageIndex = initUsers.getCurrentPage();

  prevButton.onclick = () => pageIndex>0 && initUsers.setQuery({q: this.value, page: pageIndex - 1});
  nextButton.onclick = () => pageIndex>0 && initUsers.setQuery({q: this.value, page: pageIndex + 1});

  div.appendChild(prevButton);
  div.appendChild(nextButton);

  return div;
}

const renderUsers = async (users) => {
  userListEvent.unmount();

  const usersPage = document.querySelector('.users-page');
  const usersListId = 'user-list';
  const usersListElement = createElement({
    type: 'section',
    className: 'users-list',
    id: usersListId
  });
  const usersListDiv = createElement({
    type: 'div',
    className: 'users-list-content'
  });

  users.forEach(user => {
    const div = createElement({
      type: 'div'
    });
    const p = createElement({
      type: 'p',
      innerHTML: user.login
    });
    const img = createElement({
      type: 'img',
      atr: {
        name: 'src',
        value: user.avatar_url
      }
    });
    
    div.appendChild(img);
    div.appendChild(p);
    div.onclick = () => pushUrl(`user/${user.login}`);
    usersListDiv.appendChild(div);
    
  });
  
  userListEvent.mount(usersListId);
  usersListElement.appendChild(createNextPrevButton());
  usersListElement.appendChild(usersListDiv);
  usersPage.appendChild(usersListElement);
}