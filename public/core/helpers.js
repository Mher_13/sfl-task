
createElement = ({type, className, id, innerHTML, atr}) => {
  const element = document.createElement(type);
  
  if(className)
    element.classList.add(className);
  if(innerHTML)
    element.innerHTML = innerHTML;
  if(id)
    element.id = id;
  if(atr)
    element.setAttribute(atr.name, atr.value)
  return element;
}

appendChildCustom = (parent, child) => {
  child.forEach(element => parent.appendChild(element));
}

getPathname = (pathIndex=1) => window.location.pathname.split('/')[pathIndex];
window.addEventListener('popstate', (e) => render(e));
goBack = () => window.history.back();

pushUrl = href => {
  history.pushState({}, 'test', href);
  window.dispatchEvent(new Event('popstate'));
};