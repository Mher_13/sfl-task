
const createUrl = arg => {
  let url = AppConstants.network.REST_API_URL;
  if(Array.isArray(arg)){
      url = [url, ...arg].join('/');
  }else{
      url = `${url}/${arg}`;
  }
  return url;
}

const makeAPIRequest = (url, options) => {

  let requestURL = createUrl(url);
  if(options.query){
    for(let key in options.query){
      requestURL = `${requestURL}&${key}=${options.query[key]}`;
    }
    requestURL = requestURL.replace('&', '?');
  }

  const fetchOptions = {
      method: options.method || 'GET',
      headers: {
          'Content-Type': 'application/json',
      },
  }

  return fetch(requestURL, fetchOptions)
  .then(async response => {
    
    const success = response.ok;

    if(!success){
      throw new ErrorMessage('Error message');
    }

    return response.json()
      
  })
  .then(data => data)
  .catch(err=>err);
}


makeAPIGetRequest = (url, options) => {
  options = options || {};
  options.method = 'GET';

  return makeAPIRequest(url, options);
}
