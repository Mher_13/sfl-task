componentEvent =  function() {
  let id = '';

  const changeId = val => {
    id = val;
  }

  return {
    value: () => id,
    mount: changeId,
    unmount: () => {
      const element = document.getElementById(id);

      if(element){
        element.remove();
      }
    }
  }
}
