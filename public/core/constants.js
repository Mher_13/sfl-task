AppConstants = {
  network: {
      REST_API_URL: 'https://api.github.com',
      network_request_url: {
          USERS: 'users',
          REPOS: 'repos',
          SEARCH: 'search'
      },
  },
}