const express = require("express");
const path = require("path");

const app = express();

app.use("/public", express.static(path.resolve(__dirname, "public")));

app.get("*", (req, res) => {
    res.sendFile(path.resolve("public", "index.html"));
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server has been started on port ${PORT}`));